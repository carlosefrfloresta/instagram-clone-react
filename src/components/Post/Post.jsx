import { Avatar } from "@material-ui/core";
import React from "react";
import "./Post.css";

export function Post(props) {
  return (
    <div className="post">
      <div className="post__header">
        <Avatar
          className="post__avatar"
          alt="username"
          src={`https://randomuser.me/api/portraits/${
            (props.id / 10) % 2 === 0 ? "women" : "men"
          }/${props.id}.jpg`}
        />
        <h3>Username</h3>
      </div>

      <img
        className="post__image"
        src={`https://picsum.photos/id/${props.id}/500/200`}
        alt=""
      />
      <h4 className="post__text">
        <strong>Username:</strong>
        caption
      </h4>
    </div>
  );
}
