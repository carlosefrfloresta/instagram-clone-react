import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import * as ROUTES from "./constants/routes";
import { lazy, Suspense } from "react";

const Login = lazy(() => import("./pages/Login"));

function App() {
  return (
    <Router>
      <Suspense fallback={<p>Loading...</p>} />
      <Switch>
        <Route path={ROUTES.LOGIN} component={Login} />
      </Switch>
    </Router>
  );
}

export default App;
